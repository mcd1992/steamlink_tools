// Downloads the specified steam link systemupdate zip

var fs      = require("fs");
var mkdirp  = require("mkdirp");
var request = require("request");

var download_url_regex     = /http:\/\/media\.steampowered\.com\/steamlink\/([0-9]{2}_[0-9]{4})\/(.*)/
var filename_version_regex = /SystemUpdate_full_([0-9]+).zip/

module.exports = function(url, response, callback) { // response is the HTTP/HEAD response
    var headers       = response.headers
    var etag          = headers ? headers.etag.slice(1, -1) : "no_etag"
    var urlregex      = download_url_regex.exec(url);
    var revision      = urlregex ? urlregex[1] : "other"
    var filename      = urlregex ? urlregex[2] : etag + ".zip"
    var version       = filename ? filename_version_regex.exec(filename)[1] : "0"
    var filepath      = "./SystemUpdates/" + revision + "/" + version + "/" + etag + "/"

    function download_update() {
        console.log("Downloading file: " + url + "\n\tLocal: " + filepath + filename + "\n");

        request(url, callback).pipe(
            fs.createWriteStream(filepath + filename, {
                flags: "w",
                defaultEncoding: "binary"
            } )
        );
    }

    // See if file already exists
    try {
        fs.accessSync(filepath + filename);
        callback(); // File already exists

    } catch(e) {
        // File doesn't exist

        try { // See if parent directory exists
            fs.accessSync(filepath);
            download_update(); // Parent directory exists, but not the file. Starts download

        } catch(e) {
            // Parent directory doesn't exist
            mkdirp(filepath, function(err) { // Make parent directory then start download
                download_update();
            } );
        }
    }
}
