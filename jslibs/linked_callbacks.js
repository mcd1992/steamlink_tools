//
// Library for calling an array of callbacks and then calling a
// final callback on completion of all callbacks in the array
//
// Every function MUST call callback_complete() when finished with their task
//

var eventemitter = require("events").EventEmitter; // Event system

// Returns a curried function to be called in the callback when its completed its task
function create_callback_complete(self, callback_id) {
	return function() {
		self.pending_callbacks[callback_id] = false; // Mark this callback as completed
		self.eventemitter.emit("callback_finished", callback_id); // TODO: allow return values?
	}
}

// Callbacks is an array of callbacks to call
module.exports = function(callbacks, final_callback, concurrent_callbacks) {
	concurrent_callbacks = concurrent_callbacks || 1;

    var self = {};
    self.pending_callbacks = {}; // Object of incomplete/pending callbacks
    self.eventemitter = new eventemitter(); // Event emitter for callback completions

	// A callback has finished, start one more if applicable
	self.eventemitter.on("callback_finished", function(callback_id) {
		// Startup more callbacks if they havent' ran already
		for (var index in callbacks) {
			var callable = callbacks[index];
			var pending  = self.pending_callbacks;
			var hasran   = pending[index] != undefined;

			if (!hasran) {
				var callback_complete = create_callback_complete(self, index);
				(function(callback_complete) {
					callable(callback_complete);
				})(callback_complete);

				self.pending_callbacks[index] = true; // Mark this callback as pending
				break;
			}
		}

		// Check if this is the last pending callback
		var still_waiting = false;
		for (var index in self.pending_callbacks) {
			var pending = self.pending_callbacks[index];
			if (pending) {
				still_waiting = true;
			}
		}

		// No more callbacks pending, call final_callback
		if (!still_waiting) {
			final_callback();
		}
	} );

	// Startup first callbacks
	var max_callbacks = concurrent_callbacks;
	for (var index in callbacks) {
		if (max_callbacks < 1) {
			break;
		}

		var callable = callbacks[index];
		var callback_complete = create_callback_complete(self, index);
		(function(callback_complete) {
			callable(callback_complete);
		})(callback_complete);

		self.pending_callbacks[index] = true; // Mark this callback as pending
		max_callbacks--;
	}

	// No callbacks started (empty array?)
	if (max_callbacks == concurrent_callbacks) {
		setTimeout(function() {
			final_callback(self);
		}, 1);
	}

	return self;
}
