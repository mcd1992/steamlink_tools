uint8_t magic[8]; = actually 4 bytes? 00 00 00 00

uint32_t kernel_size;  /* size in bytes */
uint32_t kernel_addr;  /* physical load addr */

uint32_t ramdisk_size; /* size in bytes */
uint32_t ramdisk_addr; /* physical load addr */

uint32_t second_size;  /* size in bytes */
uint32_t second_addr;  /* physical load addr */

uint32_t tags_addr; = all 0s
uint32_t page_size; = 00 00 00 00  01 00 00 00  00 80 00 01  EE 2E 38 0000 00 00 00  01 00 00 00  00 80 00 01  EE 2E 38 00  F0 2E 38 00  01 00 00 00  F0 2F 38 00  F0 2E 38 00
uint32_t unused[2]; = all 0s

uint8_t name[16]; = 00 00 40 00  00 00 C0 00  00 00 00 3F  00 00 00 00

uint8_t cmdline[512]; = long string


uint32_t id[8]; /* timestamp / checksum / sha1 / etc */

/* Supplemental command line data; kept here to maintain
* binary compatibility with older versions of mkbootimg */
uint8_t extra_cmdline[1024];
