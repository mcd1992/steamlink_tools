#!/usr/bin/env node
// http://media.steampowered.com/steamlink/0[6/4]_2015/[dev/public]_builds.txt

//
// Globals
//

// Steam CDN URLs and filenames
var STEAMLINK_UPDATE_URL  = "http://media.steampowered.com/steamlink/"; // hardware revision (month_year like 04_2015)
var FULL_UPDATE_FILENAME  = "SystemUpdate_full_%s.zip";                 // Software version number (like 332)
var DELTA_UPDATE_FILENAME = "SystemUpdate_delta_%s_%s.zip";             // Current version and target version numbers (like 315_332)

// Settings for brute force
var FIND_HARDWARE_REVISIONS = true; // True if we want to bruteforce look for the hardware revisions
var CONCURRENT_HEAD_REQS    = 10;   // Number of concurrent HEAD requests that can be pending at once
var CONCURRENT_DOWNLOADS    = 2;    // Number of concurrent SystemUpdate downloads at once
var REVISION_START_YEAR     = 2015; // Starting year for brute-forcing the hardware revisions
var REVISION_END_YEAR       = 2020; // Ending year for brute-forcing the hardware revisions
var SOFTWARE_VERSION_START  = 0;    // Starting number for brute-forcing the software revisions
var SOFTWARE_VERSION_END    = 999;  // Ending number for brute-forcing the software revisions


//
// Modules
//
var util             = require("util");
var request          = require("request");
var eventemitter     = require("events").EventEmitter;
var events           = new eventemitter();
var download_update  = require("./jslibs/download_update");
var linked_callbacks = require("./jslibs/linked_callbacks");


//
// Helper Functions
//

// Sends a HTTP/HEAD request to the given url
// callback is called with (err, response, body)
function HTTP_HEAD(url, callback) {
	if (url && callback) {
		request( {
			method: "HEAD",
			url:    url
		},
		callback );
	}
}


//
// Main functions
//
var eventemitter = new eventemitter(); // Event emitter for use after a stage completes
// Stages are: bruteforce possible URLs, query if those URLs return 200/OK, then download any SystemUpdates


var HARDWARE_REVISIONS = [
	"04_2015",
	"06_2015"
];
// Bruteforce potential hardware revisions and save to an array
if (FIND_HARDWARE_REVISIONS) {
	HARDWARE_REVISIONS = [];
	var head_callbacks = []; // Array of HEAD request callbacks to be passed to linked_callbacks
	for (var year = REVISION_START_YEAR; year <= REVISION_END_YEAR; year++) {
		for (var month = 1; month <= 12; month++) {
			month = (month < 10) ? ("0" + month.toString()) : month.toString();
			year  = year.toString();

			var revision = util.format("%s_%s", month, year);
			(function(revision) {
				head_callbacks.push(function(callback_complete) {
					HTTP_HEAD(STEAMLINK_UPDATE_URL + revision + "/dev_builds.txt", function(err, res, body) {
						if (!err && (res.statusCode == 200)) {
							//console.log("revision " + revision + " is valid");
							HARDWARE_REVISIONS.push(revision);
						}
						callback_complete();
					} );
				} );
			})(revision);
		}
	}

	linked_callbacks(head_callbacks, function() {
		eventemitter.emit("hardware_revisions_found");
	}, CONCURRENT_HEAD_REQS);
} else {
	setTimeout(function() {
		eventemitter.emit("hardware_revisions_found");
	}, 1);
}


// Bruteforce potential software versions and save to an array
var SYSTEM_UPDATES = {}; // Table of all the system updates that can be downloaded, the value is the http response object
eventemitter.on("hardware_revisions_found", function() {
	console.log("Hardware revisions found: ", HARDWARE_REVISIONS);

	var head_callbacks = []; // Array of HEAD request callbacks to be passed to linked_callbacks
	for (var key in HARDWARE_REVISIONS) {
		var revision = HARDWARE_REVISIONS[key];

		for (var version = SOFTWARE_VERSION_START; version <= SOFTWARE_VERSION_END; version++) {
			var fullurl = STEAMLINK_UPDATE_URL + revision + "/" + util.format(FULL_UPDATE_FILENAME, version);

			(function(fullurl) {
				head_callbacks.push(function(callback_complete) {
					HTTP_HEAD(fullurl, function(err, res, body) {
						if (!err && (res.statusCode == 200)) { // This file exists, and is available for download
							//console.log("%s is available for download.", fullurl);
							SYSTEM_UPDATES[fullurl] = res;
						}
						callback_complete();
					} );
				} );
			})(fullurl);
		}
	}

	linked_callbacks(head_callbacks, function() {
		var updates = 0;
		for (var url in SYSTEM_UPDATES) {
			updates++;
		}
		console.log("Software versions found: ", updates);

		eventemitter.emit("software_versions_found");
	}, CONCURRENT_HEAD_REQS);
} );


// Start downloading updates if we don't already have a local copy
eventemitter.on("software_versions_found", function() {
	var download_callbacks = []; // Array of DownloadSoftwareZip callbacks to be passed to linked_callbacks

	for (var url in SYSTEM_UPDATES) {
		var response = SYSTEM_UPDATES[url];

		(function(url, response) {
			download_callbacks.push(function(callback_complete) {
				download_update(url, response, function() {
					callback_complete();
				} );
			} );
		})(url, response);
	}

	linked_callbacks(download_callbacks, function() {
	}, CONCURRENT_DOWNLOADS);
} );
