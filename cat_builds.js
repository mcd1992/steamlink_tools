#!/usr/bin/env node

// http://media.steampowered.com/steamlink/06_2015/public_builds.txt

var request      = require("request");             // HTTP request system
var printf       = require("printf");              // util.format is useless...
var eventemitter = require("events").EventEmitter; // Event system for callbacks

var emitter       = new eventemitter(); // Event emitter for http request callbacks
var public_builds = {};
var beta_builds   = {};
var dev_builds    = {};

var pending_callbacks = { // Object of callbacks still pending
    "public": true,
    "beta":   true,
    "dev":    true
}

// Marks a branch version file as download complete and emits final event if all events are done
function mark_completion(branch) {
	if (branch) {
		pending_callbacks[branch] = false;

		var finished = true;
		for (var index in pending_callbacks) {
			var value = pending_callbacks[index];
			if (value == true) {
				finished = false;
				break;
			}
		}
		if (finished) {
			emitter.emit("generate_table");
		}
	}
}

// Takes the string blob from a request to the steam builds.txt and returns an array of the builds
function blob_to_object(blob) {
	var obj = {};

	if (blob) {
		var versions = blob.split(/\r?\n/);
		if (versions) {
			for (var key = 0; key < versions.length; key++) {
				var value = versions[key];
				if (value) {
					obj[value] = true;
				}
			}
		}
	}

	return obj;
}

// Request public builds
request("http://media.steampowered.com/steamlink/06_2015/public_builds.txt", function(err, res, body) {
	if (!err) {
		public_builds = blob_to_object(body);
	}

	mark_completion("public");
} );

// Request beta builds
request("http://media.steampowered.com/steamlink/06_2015/beta_builds.txt", function(err, res, body) {
	if (!err) {
		beta_builds = blob_to_object(body);
	}

	mark_completion("beta");
} );

// Request dev builds
request("http://media.steampowered.com/steamlink/06_2015/dev_builds.txt", function(err, res, body) {
	if (!err) {
		dev_builds = blob_to_object(body);
	}

	mark_completion("dev");
} );


// Wait for all http request callbacks to complete
emitter.on("generate_table", function() {
	console.log(printf("%-14.14s %-14.14s %-14.14s", "Public Builds:", "Beta Builds:", "Dev Builds:"));

	for (version = 0; version < 999; version++) {
        var version_str = version.toString();
        var ispublic    = (public_builds[version_str] == true);
        var isbeta      = (beta_builds[version_str] == true);
        var isdev       = (dev_builds[version_str] == true);

		if (ispublic || isbeta || isdev) {
			console.log(
				printf("%-14.14s %-14.14s %-14.14s",
					ispublic ? version_str : "---",
					isbeta   ? version_str : "---",
					isdev    ? version_str : "---"
				)
			);
		}
	}
} );
